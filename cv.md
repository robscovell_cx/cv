# Curriculum Vitae: Robert Scovell
* Citizenship: UK and NZ
## Education
* BSc Physical Science, University of Edinburgh 1991
* BD Hons Systematic Theology, University of Edinburgh 1995
* PGCE (Adult education and training), University of Wales 1996
* Level 3 modules in Mathematical Modelling and Computational Maths techiques, 1996-1998
## Contract Work Experience (reverse order)
* [Prayer Letters](https://www.prayerletters.com/) 2020 -- present
* [Company-X](https://companyx.com/) 2012 -- present
* [Vonage UK](https://www.vonage.co.uk/) 2014 -- present
* [Hi Voicemail] (https://hivoicemail.com/) 2016
* [MEA](https://we-are-mea.com/) 2007 -- 2009
* [Coms plc](https://www.timico.com/) (former VoIP company subsequently taken over by Timico) 2004 -- 2014
* [Collins Dictionaries](https://www.collinsdictionary.com/) 1999 -- 2004
* [National Centre for Prosthetics and Orthotics](https://www.strath.ac.uk/courses/undergraduate/prostheticsorthotics/) 1998 -- 1999
* [Pfizer UK](https://www.pfizer.co.uk/) 1997 -- 1998
## Relevant Project experience
* General: I work with Company-X supporting a number of legacy applications developed in  Perl, PHP, MySQL, PostgreSQL, MS SQL, Delphi etc.
* Hi Voicemail: I built a voicemail server using Node.js and Freeswitch.
* Vonage UK: I support, maintain and develop a set of Perl applications that are mission-critical for the company's VOIP operations. 
* Coms plc: from 2004 to 2014 I developed, supported and maintained a VOIP billing system written using Perl/Mason.
* Troy Reasearch: from 2004 to 2007 I developed, supported and maintained OO-Perl and embperl code.
* Collins Dictionaries: from 1999 to 2004 I worked for Collins Dictionaries on Perl text processing pipelines for typesetting dictionaries from tagged text through to PostScript.
* National Centre for Prosthetics and Orthotics: Delphi, BDE and Paradox application for hospital amputee treatment data gathering. SPSS for statistical analysis.
* Pfizer Pharmaceuticals UK: my first contract was writing test scripts in Fortran and Genstat. I used Perl to process output from the test scripts into RTF to create Word documents.
## Contract Modus Operandi
Since 2012 I have specialised in supporting large, complex 'legacy' software. It's a field that many shy away from but it's a challenge I enjoy. I work remotely in both New Zealand and the UK and spend time in both countries.



